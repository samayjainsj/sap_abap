REPORT z.

TYPES : BEGIN OF ty_sflight,
          carrid TYPE sflight-carrid,
          connid TYPE sflight-connid,
          fldate TYPE sflight-fldate,
          price  TYPE sflight-price,
        END OF ty_sflight.

DATA : it_sflight TYPE TABLE OF ty_sflight, "SFLIGHT internal table
       wa_sflight TYPE ty_sflight.
DATA : it_fcat_sflight TYPE slis_t_fieldcat_alv. "SFLIGHT field catalog
DATA : wa_fcat_sflight LIKE LINE OF it_fcat_sflight.

START-OF-SELECTION.

  PERFORM get_data_sflight.
  PERFORM create_fcat_sflight.
  PERFORM disp_alv_sflight.


FORM get_data_sflight.
  SELECT  carrid connid
              fldate
              price FROM sflight INTO TABLE it_sflight.  "get flight data
"       FOR ALL ENTRIES IN it_spfli WHERE carrid = it_spfli-carrid.
ENDFORM.

FORM create_fcat_sflight.
***build fcat for SFLIGHT
  wa_fcat_sflight-col_pos       = '1'.
  wa_fcat_sflight-fieldname     = 'CARRID'.
  "W_FCAT_SFLIGHT-TABNAME       = 'IT_SFLIGHT'.
  wa_fcat_sflight-ref_tabname   = 'SFLIGHT'.
  wa_fcat_sflight-fieldname = 'CARRID'.
  APPEND wa_fcat_sflight TO it_fcat_sflight.
  CLEAR wa_fcat_sflight.

  wa_fcat_sflight-col_pos       = '2'.
  wa_fcat_sflight-fieldname     = 'CONNID'.
  "W_FCAT_SFLIGHT-TABNAME       = 'IT_SFLIGHT'.
  wa_fcat_sflight-ref_tabname   = 'SFLIGHT'.
    APPEND wa_fcat_sflight TO it_fcat_sflight.
  CLEAR wa_fcat_sflight.

  wa_fcat_sflight-col_pos       = '3'.
  wa_fcat_sflight-fieldname     = 'FLDATE'.
  "W_FCAT_SFLIGHT-TABNAME       = 'IT_SFLIGHT'.
  wa_fcat_sflight-ref_tabname   = 'SFLIGHT'.
  wa_fcat_sflight-ref_fieldname = 'FLDATE'.
  APPEND wa_fcat_sflight TO it_fcat_sflight.
  CLEAR wa_fcat_sflight.

  wa_fcat_sflight-col_pos       = '3'.
  wa_fcat_sflight-fieldname     = 'PRICE'.
  "W_FCAT_SFLIGHT-TABNAME       = 'IT_SFLIGHT'.
  wa_fcat_sflight-ref_tabname   = 'SFLIGHT'.
  wa_fcat_sflight-ref_fieldname = 'PRICE'.
  APPEND wa_fcat_sflight TO it_fcat_sflight.
  CLEAR wa_fcat_sflight.


ENDFORM.


FORM disp_alv_sflight.
  CALL FUNCTION 'REUSE_ALV_GRID_DISPLAY'
    EXPORTING
      it_fieldcat        = it_fcat_sflight "PASS FIELD CATALOG TO ALV
    TABLES
      t_outtab           = it_sflight.
ENDFORM.