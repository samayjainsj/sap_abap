REPORT z.
TABLES spfli. "DECLARE TABLE FOR SELECT-OPTIONS
DATA it_spfli TYPE TABLE OF spfli. "DECLARE INTERNAL TABLE FOR SPFLI
SELECT-OPTIONS: s_carrid FOR spfli-carrid. "PRINT SELECT-OPTIONS FOR MATNR

START-OF-SELECTION.
  SELECT * FROM spfli INTO TABLE it_spfli
  WHERE carrid IN s_carrid .
  CALL FUNCTION 'REUSE_ALV_GRID_DISPLAY' "CALL FUNCTION MODULE TO DISPLAY ALV GRID
    EXPORTING
      i_structure_name = 'SPFLI'
      i_grid_title     = 'This is grid display' " optional parameter
    TABLES
      t_outtab         = it_spfli. "PASS INTERNAL TABLE TO DISPLAY ALV FORMAT

______________________________________________

REPORT z.
TABLES : SPFLI. "DECLARE TABLE FOR SELECT-OPTIONS
DATA : IT_SPFLI TYPE TABLE OF SPFLI. "DECLARE INTERNAL TABLE FOR SPFLI
SELECT-OPTIONS: S_CARRID FOR SPFLI-CARRID. "PRINT SELECT-OPTIONS FOR MATNR
START-OF-SELECTION.
SELECT * FROM SPFLI INTO TABLE IT_SPFLI
                   WHERE CARRID IN S_CARRID .
CALL FUNCTION 'REUSE_ALV_LIST_DISPLAY' "CALL FUNCTION MODULE TO DISPLAY ALV GRID
EXPORTING
I_STRUCTURE_NAME = 'SPFLI'
TABLES
T_OUTTAB = IT_SPFLI. "PASS INTERNAL TABLE TO DISPLAY ALV FORMAT