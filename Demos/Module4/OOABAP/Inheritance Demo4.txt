REPORT  z.
CLASS employee DEFINITION.
  PUBLIC SECTION.
    METHODS:
      constructor
        IMPORTING im_employee_no   TYPE i
                  im_employee_name TYPE string,
      display_attributes.
  PRIVATE SECTION.
    DATA: no   TYPE i,
          name TYPE string.
ENDCLASS.
*--- Employee - Implementation
CLASS employee IMPLEMENTATION.
  METHOD  constructor.
    no = im_employee_no.
    name = im_employee_name.
  ENDMETHOD.

  METHOD display_attributes.
    WRITE:/ 'Employee', no, name.
  ENDMETHOD.
ENDCLASS.

*******************************************************
CLASS bluecollar_employee DEFINITION
           INHERITING FROM employee.
  PUBLIC SECTION.
    METHODS:
      constructor    IMPORTING
                      im_employee_no   TYPE i
                      im_employee_name TYPE string
                      im_hours         TYPE i,
      display_attributes REDEFINITION.
  PRIVATE SECTION.
    DATA : hours          TYPE i.
ENDCLASS.

*---- CLASS BlueCollar_Employee IMPLEMENTATION
CLASS bluecollar_employee IMPLEMENTATION.
  METHOD  constructor.
    super->constructor( im_employee_no = im_employee_no
                        im_employee_name = im_employee_name ).
    hours = im_hours.
  ENDMETHOD.

  METHOD display_attributes.
    super->display_attributes( ).
    WRITE hours.
  ENDMETHOD.
ENDCLASS.

DATA:
* Object references
  o_bluecollar_employee TYPE REF TO bluecollar_employee,
  employee              TYPE REF TO employee.



START-OF-SELECTION.
  CREATE OBJECT employee
    EXPORTING
      im_employee_no   = 1
      im_employee_name = 'Gylle Karen'.
  CALL METHOD employee->display_attributes.
* Create bluecollar employee obeject
  CREATE OBJECT o_bluecollar_employee
    EXPORTING
      im_employee_no   = 2
      im_employee_name = 'Tom'
      im_hours         = 5.
  CALL METHOD o_bluecollar_employee->display_attributes.