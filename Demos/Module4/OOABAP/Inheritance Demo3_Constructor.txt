REPORT  z.
CLASS employee DEFINITION.
  PUBLIC SECTION.
    METHODS:
      constructor
        IMPORTING im_employee_no   TYPE i
                  im_employee_name TYPE string,
      display_attributes.
  PRIVATE SECTION.
*--------------------------------------------------------------------
    DATA: no   TYPE i,
          name TYPE string.
ENDCLASS.
*--- Employee - Implementation
CLASS employee IMPLEMENTATION.
  METHOD  constructor.
    no = im_employee_no.
    name = im_employee_name.
  ENDMETHOD.

  METHOD display_attributes.
    WRITE:/ 'Employee', no, name.
  ENDMETHOD.
ENDCLASS.

*******************************************************
CLASS bluecollar_employee DEFINITION
           INHERITING FROM employee.
  PUBLIC SECTION.
    METHODS:
      "constructor REDEFINITION,
      " constructor cannot be over defined
      constructor   IMPORTING
                      im_employee_no   TYPE i
                      im_employee_name TYPE string,
      display_attributes REDEFINITION.
  PRIVATE SECTION.
    DATA:no   TYPE i,
         name TYPE string.
ENDCLASS.

*---- CLASS BlueCollar_Employee IMPLEMENTATION
CLASS bluecollar_employee IMPLEMENTATION.
  METHOD  constructor.
    CALL METHOD super->constructor(
        im_employee_no   = im_employee_no
        im_employee_name = im_employee_name ).
  ENDMETHOD.

  METHOD display_attributes.
    super->display_attributes( ).
  ENDMETHOD.
ENDCLASS.

DATA:
* Object references
  o_bluecollar_employee1  TYPE REF TO bluecollar_employee.

START-OF-SELECTION.
* Create bluecollar employee obeject
  CREATE OBJECT o_bluecollar_employee1
    EXPORTING
      im_employee_no   = 1
      im_employee_name = 'Gylle Karen'.
  CALL METHOD o_bluecollar_employee1->display_attributes.