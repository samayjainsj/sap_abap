REPORT  z.
DATA orf_1 TYPE REF TO cx_root.
DATA: num1 TYPE i,
      num2 TYPE i,
      res  TYPE i.
PERFORM setnumbers.

TRY.
    PERFORM calc.

  CATCH cx_sy_zerodivide INTO orf_1.
    WRITE 'Exception occurred'.
    WRITE / orf_1->kernel_errid.
    WRITE / orf_1->get_text( ).
    WRITE / orf_1->get_longtext( ).
ENDTRY.


PERFORM displaynumbers.

FORM setnumbers.
  num1 = 10.
  num2 = 0.
ENDFORM.

FORM calc RAISING cx_sy_zerodivide.
  res = num1 / num2.
ENDFORM.

FORM displaynumbers.
  WRITE:/ 'Res is:' , res.
ENDFORM.