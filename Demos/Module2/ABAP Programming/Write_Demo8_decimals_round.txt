REPORT z.
DATA: F1    TYPE  P  DECIMALS 3 VALUE '1575.456'.
WRITE:      /  F1,      20 'as-is',       "output is 1,575.456
           / F1  DECIMALS 1,	20 'decimals 1', 	          "output is 1,575.5
              / F1  DECIMALS 0,	20 'decimals 0', 	          "output is 1,575
              / F1  DECIMALS -1,  20 'decimals  -1',            "output is 158
*
              / F1  ROUND  4,   20 'round  4',        "output is 0.158
              / F1  ROUND  3,   20 'round  3',        "output is 1.575
              / F1  ROUND  2,   20 'round  2',        "output is 15.755
              / F1  ROUND  1,   20 'round  1',        "output is 157.546
              / F1  ROUND  0,   20 'round  0',        "output is 1,575.456
              / F1  ROUND  -1,  20 'round  -1',             "output is 15,754.560
              / F1  ROUND  -2,  20 'round  -2',              "output is 157,546.600
              / F1  ROUND  3 DECIMALS 1, 20 'round  3 decimals 1',  "output is 1.6
              / F1  ROUND  3 DECIMALS 3, 20 'round  3 decimals 3'.  "output is 1.575