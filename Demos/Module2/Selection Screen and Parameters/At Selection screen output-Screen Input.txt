REPORT Z.
PARAMETERS:EMPNO(4)  TYPE N,
           ENAME(10) TYPE C.

AT SELECTION-SCREEN OUTPUT.
  LOOP AT SCREEN.
    IF SCREEN-NAME = 'ENAME'.
       SCREEN-INPUT = 0. "Cannot enter values if screen-input is 0
      MODIFY SCREEN.
    ENDIF.
  ENDLOOP.

START-OF-SELECTION.
  WRITE :/ 'Ename is :', ENAME.
  WRITE :/ 'Empno is :', EMPNO.