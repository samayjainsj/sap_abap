REPORT z.
PARAMETERS: num1  TYPE  i,
            num2  TYPE  i.

AT SELECTION-SCREEN.
    PERFORM validatenum1num2.
   
START-OF-SELECTION.
WRITE  num1.
WRITE  num2.

FORM validatenum1num2.
  IF num1 < 0 or num2 < 10.
    MESSAGE E001(Z104329MSGCLS)
    WITH 'Enter proper values for Num1 and Num2'.
  ENDIF.
ENDFORM.



