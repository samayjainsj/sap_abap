REPORT z.
data itempdept type STANDARD TABLE OF Z104329DBVIEW1.
data waempdept type Z104329DBVIEW1.

SELECT mandt EMPNO ENAME JOB SAL DEPTNO
  DEPTNO1 DNAME LOC
  INTO TABLE ITEMPDEPT
  FROM Z104329DBVIEW1.

  loop at itempdept INTO waempdept.
    write : / waempdept-empno,
              waempdept-ename,
              waempdept-job,
              waempdept-sal,
              waempdept-deptno,
              waempdept-deptno1,
              waempdept-dname,
              waempdept-loc.
  endloop.
