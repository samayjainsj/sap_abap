report zvk1.


data: f1 value 'A',
      f2 value 'B'.

write: / f1, f2.
perform s1 using f1
           changing f2.
write: / f1, f2.

*form s1 USING p1 p2.
form s1 USING value(p1) value(p2)
*form s1 USING value(p1) p2.
   p1 = 'X'.
   p2 = 'Y'.
endform.

*form s1 USING value(p1) CHANGING value(p2).
*form s1 USING value(p1) CHANGING p2.
*   p1 = 'X'.
*   p2 = 'Y'.
*endform.

*form s1 CHANGING value(p1) value(P2).
*form s1 CHANGING value(p1) p2.
*   p1 = 'X'.
*   p2 = 'Y'.
*endform.

*form s1 CHANGING value(p1) using P2.
*   p1 = 'X'.
*   p2 = 'Y'.
*endform.



*In the above case I am passing f2 by reference(changing)
*and also receiving it by reference. Any change made 
*p2 in subroutine s1 will be available later.
*
*However I am passing f1 by reference(using)
*but also receiving it by value(using value). Any change made 
*in p1 in form s1 will NOT be available later.
 
OUTPUT
A B
A Y


If in the above case while receiving, If I have coded
form s1 using p1.. it would have indicated that I am also
receiving by reference and any change made in p1 in subroutine s1
will be available later.




report zvk1.
 
data: f1 value 'A',
      f2 value 'B'.

  write: / f1, f2.
  perform s1 using f1 f2.
  write: / f1, f2.

 form s1 using value(p1) value(p2). 
p1 = 'Y'.
p2 = 'X'.
     endform.