*&---------------------------------------------------------------------*
*& Module Pool       Z104329MODULEPOOL3
*&
*&---------------------------------------------------------------------*
*&
*&
*&---------------------------------------------------------------------*


INCLUDE Z104329MODULEPOOL4TOP.
*INCLUDE Z104329MODULEPOOL3TOP                   .    " global Data

* INCLUDE Z104329MODULEPOOL3O01                   .  " PBO-Modules
* INCLUDE Z104329MODULEPOOL3I01                   .  " PAI-Modules
* INCLUDE Z104329MODULEPOOL3F01                   .  " FORM-Routines

*&---------------------------------------------------------------------*
*&      Module  USER_COMMAND_0100  INPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE USER_COMMAND_0100 INPUT.
OK_CODE = SY-UCOMM.
SELECT SINGLE * FROM Z104329DEPT
    WHERE DEPTNO = Z104329DEPT-DEPTNO.
CASE OK_CODE.
  WHEN 'CREATE'.
       PERFORM CREATEPARA.
  WHEN 'DISPLAY'.
       PERFORM DISPLAYPARA.
  WHEN 'EXIT'.
       LEAVE PROGRAM.
 ENDCASE.
ENDMODULE.

FORM CREATEPARA.
IF SY-SUBRC = 0.
   MESSAGE I003(Z104329MSGCLASS).
   CALL SCREEN 100.
ELSE.
   MESSAGE I004(Z104329MSGCLASS).
   CALL SCREEN 200.
ENDIF.
ENDFORM.

FORM DISPLAYPARA.
IF SY-SUBRC = 0.
   MESSAGE I003(Z104329MSGCLASS).
   CALL SCREEN 200.
ELSE.
   MESSAGE I004(Z104329MSGCLASS).
   CALL SCREEN 100.
ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Module  USER_COMMAND_0200  INPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE USER_COMMAND_0200 INPUT.
CASE SY-UCOMM.
  WHEN 'BACK'.
       CALL SCREEN 100.
  WHEN 'SAVE'.
       PERFORM INSERTPARA.
 ENDCASE.
ENDMODULE.

FORM INSERTPARA.

INSERT INTO Z104329DEPT VALUES Z104329DEPT.
 IF SY-SUBRC = 0.
      MESSAGE I005(Z104329MSGCLASS) WITH 'Record Inserted'.
   ELSE.
      MESSAGE I006(Z104329MSGCLASS) WITH 'Record Not Inserted'.
ENDIF.
ENDFORM.
*&---------------------------------------------------------------------*
*&      Module  STATUS_0200  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE STATUS_0200 OUTPUT.
* SET PF-STATUS 'STAT1'.
*  SET TITLEBAR 'xxx'.
LOOP AT  SCREEN.
  IF SCREEN-NAME = 'Z104329DEPT-DEPTNO'.
      SCREEN-INPUT = 0.
      MODIFY SCREEN.
     ENDIF.
ENDLOOP.

CASE OK_CODE.
WHEN 'CREATE'.
     Z104329DEPT-DNAME = ' '.
     Z104329DEPT-LOC = ' '.
WHEN 'DISPLAY'.
  LOOP AT SCREEN.
    IF SCREEN-NAME = 'Z104329DEPT-DNAME'
      OR SCREEN-NAME = 'Z104329DEPT-LOC'.
      SCREEN-INPUT = 0.
    ENDIF.
   MODIFY SCREEN.
  ENDLOOP.
ENDCASE.
ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  STATUS_0100  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE STATUS_0100 OUTPUT.
  SET PF-STATUS 'Z104329STAT1'.
*  SET TITLEBAR 'xxx'.
ENDMODULE.