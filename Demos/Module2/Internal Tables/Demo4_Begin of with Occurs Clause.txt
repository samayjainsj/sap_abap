REPORT Z.
*begin of with occurs clause
*creates default header line
"In case you have an internal table with header line
"A work area can still be created
DATA : BEGIN OF STUD1  OCCURS 0 ,
         ROLLNO   TYPE I,
         NAME(10) TYPE C,
       END OF STUD1.
"DATA: WA1 LIKE  STUD1.
DATA: WA1 LIKE  LINE OF STUD1.

WA1-ROLLNO = 10. WA1-NAME = 'Ram'.
APPEND WA1 TO STUD1.

WA1-ROLLNO = 20. WA1-NAME = 'Shyam'.
APPEND WA1 TO STUD1.

WA1-ROLLNO = 30. WA1-NAME = 'Krishna'.
APPEND WA1 TO STUD1.

LOOP AT STUD1 INTO WA1.
  WRITE : / SY-TABIX, WA1-ROLLNO , WA1-NAME.
ENDLOOP.


