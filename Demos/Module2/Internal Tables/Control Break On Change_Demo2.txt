REPORT Z.
TYPES: BEGIN OF TYEMP,
         EMPNO  TYPE Z104329EMP-EMPNO,
         ENAME  TYPE Z104329EMP-ENAME,
         DEPTNO TYPE Z104329EMP-DEPTNO,
         SAL    TYPE Z104329EMP-SAL,
       END OF TYEMP.

DATA: ITEMP1 TYPE  STANDARD
      TABLE OF TYEMP INITIAL SIZE 0,
      WA     LIKE LINE OF ITEMP1.

SELECT EMPNO ENAME DEPTNO SAL
  FROM Z104329EMP
  INTO TABLE ITEMP1.

SORT ITEMP1 BY DEPTNO.
* Need to sort on control field
LOOP AT ITEMP1  INTO WA.
*  AT NEW DEPTNO.
 ON CHANGE OF WA-DEPTNO.
    WRITE : / WA-DEPTNO.
ENDON.
*  ENDAT.
    WRITE:  / WA-EMPNO, WA-ENAME,WA-SAL.

ENDLOOP.