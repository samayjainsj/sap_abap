REPORT z.
DATA ITAB TYPE TABLE OF I WITH HEADER LINE.

"When Internal table is declared with a Header Line
"the name of Internal table and  the name of Header Line 
"is the same. Header line and workarea imply the same.
ITAB = 10. APPEND ITAB TO ITAB.
ITAB = 20. APPEND ITAB.
ITAB = 30. APPEND ITAB.
ITAB = 40. APPEND ITAB.
ITAB = 50. APPEND ITAB.

"LOOP AT ITAB INTO ITAB.
LOOP AT ITAB.
  WRITE:/ ITAB.
ENDLOOP.

______________________________________________

