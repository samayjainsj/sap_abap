REPORT zvk3 NO STANDARD PAGE HEADING LINE-SIZE 70 LINE-COUNT 21(2) .

TABLES: mara,                          " Material master
   	eban,                          " Purchase requisition
       ekko,                          "Purchase Document Header
       ekpo.                          " Purchasing Document Item

START-OF-SELECTION.
 SELECT * FROM mara.
  WRITE:/ mara-matnr UNDER 'Material No' HOTSPOT,
                mara-mtart UNDER 'Material Type',
                mara-meins UNDER 'Measure Unit'.

  HIDE mara-matnr.
  ULINE.
  ENDSELECT.

TOP-OF-PAGE.

  WRITE:/2 'Material No'      CENTERED COLOR COL_HEADING,
                25 'Material Type' CENTERED COLOR COL_HEADING,
                50 'Material Unit'  CENTERED COLOR COL_HEADING.

  ULINE.
*
TOP-OF-PAGE DURING LINE-SELECTION.
*
  IF sy-lsind = '1'.
    WRITE: /2 'Pur Req No' COLOR COL_KEY,
                 20 'Doc Type '    COLOR COL_KEY,
                 40 'Plant' COLOR  COL_KEY,
                 60 'Storage LOC'  COLOR COL_KEY.

    ULINE.

  ELSEIF sy-lsind = '2'.

    WRITE: /2 'Pur Doc No' COLOR COL_KEY,
                25 'Company Code ' COLOR COL_KEY.


  ENDIF.
*
END-OF-PAGE.
    ULINE.
    WRITE: /50 'Page No', sy-pagno.
*
AT LINE-SELECTION.
 IF sy-lsind = '1'.

    SELECT * FROM eban
                      WHERE matnr = mara-matnr.

      WRITE: / eban-banfn UNDER 'Pur Req No' ,
                      eban-bsart UNDER 'Doc Type',
                      eban-werks UNDER 'Plants',
                      eban-lgort UNDER 'Storage Loc'.
      HIDE eban-lifnr.

    ENDSELECT.
  ELSEIF sy-lsind = '2'.

    SELECT * FROM ekko
                      WHERE
                           lifnr = eban-lifnr.

      WRITE: / ekko-ebeln UNDER 'Pur Doc No' ,
                       ekko-bukrs UNDER 'Company Code'.
    ENDSELECT.
ENDIF.
