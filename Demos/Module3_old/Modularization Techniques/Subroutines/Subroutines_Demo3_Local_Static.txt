REPORT  ZVKPRG4.
PERFORM para1.
 "WRITE: / var1, var2. "var1 var2 will not be available
                      "here
PERFORM para1.

FORM para1.
  DATA    lvar1  TYPE i VALUE 10.
  STATICS svar2  TYPE i VALUE 10.
  lvar1 = lvar1  + 1.
  svar2 = svar2 + 1.
  WRITE: / lvar1, svar2.
ENDFORM.
_________________________________________


REPORT  ZVKPRG4.
DO 10 TIMES. 
  PERFORM add_one. 
ENDDO. 

FORM add_one. 
  DATA    local  TYPE i VALUE 10. 
  STATICS static TYPE i VALUE 10. 
  local  = local  + 1. 
  static = static + 1. 
  WRITE: / local, static. 
ENDFORM. 