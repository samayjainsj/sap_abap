report z.
data: f1(10) type c value 'Before'.
write: / 'Before invoking s2: f1 =', f1.
perform s2 using f1.
*perform s2 changing f1.
write: / 'After invoking  s2: f1 =', f1.

end-of-selection.
      write: / 'in EOS Stopped. f1 =', f1.

form s2 changing value(p1).
"form s2 USING value(p1).  "pass by value
*Above is pass by value and result  
*Observe the code by commenting stop  
     p1 = 'After'.
*stop statement terminates the subroutine and goes to end-of-selection
     stop.
endform.